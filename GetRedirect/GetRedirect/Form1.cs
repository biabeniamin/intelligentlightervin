﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GetRedirect
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async Task<string> HttpGet(string url)
        {
            string response = "";
            HttpClient client = new HttpClient();

            try
            {
                response = await client.GetStringAsync(url);
            }
            catch(Exception er)
            {

            }

            return response;
        }

        private async void Redirect(string url1, string url2)
        {
            while (true)
            {
                string response = await HttpGet(url1);

                if("-1" != response)
                {
                    if (response == "1")
                    {
                        await HttpGet(url2 + "ffffff&r=1");
                        await HttpGet(url2 + "ffffff&r=2");
                    }
                    else if (response == "0")
                    {
                        await HttpGet(url2 + "000000&r=1");
                        await HttpGet(url2 + "000000&r=2");
                    }
                    else if (response == "3")
                        await HttpGet(url2 + "ffffff&r=1");
                    else if (response == "2")
                        await HttpGet(url2 + "000000&r=1");
                    else if (response == "5")
                        await HttpGet(url2 + "ffffff&r=2");
                    else if (response == "4")
                        await HttpGet(url2 + "000000&r=2");
                    else if(response.Contains("h"))
                    {
                        await HttpGet(url2 + response.Substring(2) + "&r=" + response.Substring(0,1));
                    }
                }

                System.Threading.Thread.Sleep(1000);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Redirect("http://automations.avramiancuturda.ro/getAction.php", textBox1.Text);
        }
    }
}
