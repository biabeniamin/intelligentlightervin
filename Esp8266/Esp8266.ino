#include <ESP8266WiFi.h>
#include "Webserver.h"
#include "HtmlBuilder.h"
#include "Light.h"

void NewHttpClient();

Webserver server(NewHttpClient);
Light light;
HtmlBuilder htmlBuilder(&light);


int rColor;
int gColor;
int bColor;


void setup() {
  Serial.begin(9600);

  server.Start(&htmlBuilder);
  light.UpdateRoom1(0, 0, 0);
  light.UpdateRoom2(0, 0, 0);
}

int ConvertHexaDigit(char c)
{
  if ('a' <= c)
  {
    return c - 'a' + 10;
  }

  return c - '0';
}

int ConvertHexaToDeci(String numb)
{
  return ConvertHexaDigit(numb.charAt(0))  * 16 + ConvertHexaDigit(numb.charAt(1));
}

void loop()
{
  String colorR;
  String colorG;
  String colorB;


  server.CheckForConnectedClients();
  light.CheckTimer();

  colorR = server.GetParameter("colorR=");
  if (!colorR.equals(""))
  {
    rColor = colorR.toInt();
  }

  colorG = server.GetParameter("colorG=");
  if (!colorG.equals(""))
  {
    gColor = colorG.toInt();
  }

  colorB = server.GetParameter("colorB=");
  if (!colorB.equals(""))
  {
    bColor = colorB.toInt();
  }



}

void NewHttpClient()
{
  String colorH;
  String room;

  room = server.GetParameter("r=");
  colorH = server.GetParameter("colorH=");
  if (!colorH.equals(""))
  {
    rColor = ConvertHexaToDeci(colorH.substring(0, 2)) ;
    gColor = ConvertHexaToDeci(colorH.substring(2, 4)) ;
    bColor = ConvertHexaToDeci(colorH.substring(4, 6)) ;
    Serial.println(rColor);
    Serial.println(gColor);
    Serial.println(bColor);

    if (room.equals("1"))
    {
      light.UpdateRoom1(rColor, gColor, bColor);
    }
    else if (room.equals("2"))
    {
      light.UpdateRoom2(rColor, gColor, bColor);
    }
  }


}


