#include "Light.h"


Light::Light()
{
	_cRoom1 = { .red = 128,.green = 64,.blue = 32 };
	_cRoom2 = { .red = 0,.green = 0,.blue = 0 };

	FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(_leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
	FastLED.setBrightness(BRIGHTNESS);
}

void Light::Update()
{
	FastLED.show();
}

void Light::UpdateRoom1(int r, int g, int b)
{
	_cRoom1 = { .red = r,.green = g,.blue = b };
	for (int i = 0; i < NUM_LEDS_ROOM1; i++) {
		_leds[i].red = _cRoom1.red;
		_leds[i].green = _cRoom1.green;
		_leds[i].blue = _cRoom1.blue;
	}

	Update();
}

void Light::UpdateRoom2(int r, int g, int b)
{
	_cRoom2 = { .red = r,.green = g,.blue = b };
	for (int i = NUM_LEDS_ROOM1; i < NUM_LEDS; i++) {
		_leds[i].red = _cRoom2.red;
		_leds[i].green = _cRoom2.green;
		_leds[i].blue = _cRoom2.blue;
	}

	Update();
}

char GetHexaDigitFromBits(char bits)
{
	if (10 > bits)
	{
		return '0' + bits;
	}

	return ('a' + bits - 10);
}

String GetHexaNumber(COLOR color)
{
	String num = "#";

	num += GetHexaDigitFromBits(color.red >> 4);
	num += GetHexaDigitFromBits(color.red & 0xF);
	
	num += GetHexaDigitFromBits(color.green >> 4);
	num += GetHexaDigitFromBits(color.green & 0xF);

	num += GetHexaDigitFromBits(color.blue >> 4);
	num += GetHexaDigitFromBits(color.blue & 0xF);

	Serial.println(num);
	return num;
}

String Light::GetColorRoom1()
{
	return GetHexaNumber(_cRoom1);
}

String Light::GetColorRoom2()
{
	return GetHexaNumber(_cRoom2);
}

void Light::Blink()
{
	if (1 == _isTimerOn)
	{
		return;
	}

	_cInitialRoom1 = _cRoom1;
	_cInitialRoom2 = _cRoom2;

	UpdateRoom1(_cInitialRoom1.red ^ 0xFF, 0x0, 0x0);
	UpdateRoom2(_cInitialRoom2.red ^ 0xFF, 0x0, 0x0);

	_isTimerOn = 1;
	_interval = 1000;
	_start = millis();
	Update();
}

void Light::CheckTimer()
{
	if (1 == _isLightResponsiveOn)
	{
		return CheckLightIntesity();
	}

	if (1 != _isTimerOn)
	{
		return;
	}

	if (_interval > (millis() - _start))
	{
		return;
	}

	_isTimerOn = 0;
	UpdateRoom1(_cInitialRoom1.red, _cInitialRoom1.green, _cInitialRoom1.blue);
	UpdateRoom2(_cInitialRoom2.red, _cInitialRoom2.green, _cInitialRoom2.blue);
	Update();


}

void Light::StartLightResponsive()
{
	Serial.println("light responsive activated");
	_isLightResponsiveOn = 1;

	if (0 == _cRoom1.red
		&& 0 == _cRoom1.green
		&& 0 == _cRoom1.blue)
	{
		UpdateRoom1(0xFF, 0xFF, 0xFF);
	}

	if (0 == _cRoom2.red
		&& 0 == _cRoom2.green
		&& 0 == _cRoom2.blue)
	{
		UpdateRoom2(0xFF, 0xFF, 0xFF);
	}
}

void Light::StopLightResponsive()
{
	_isLightResponsiveOn = 0;
	FastLED.setBrightness(BRIGHTNESS);
	FastLED.show();
}

void Light::CheckLightIntesity()
{
	byte value;

	value = map(analogRead(0), 900, 0, 0, 50);
	FastLED.setBrightness(value);
	FastLED.show();

}