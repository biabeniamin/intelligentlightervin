#include "HtmlBuilder.h"


HtmlBuilder::HtmlBuilder(Light *light)
{
	_light = light;
}


char* HtmlBuilder::GetHtml(String url)
{
	String html;

	html = "";

	if (-1 != url.indexOf("/color1"))
	{
		html = _light->GetColorRoom1();
	}
	else if (-1 != url.indexOf("/color2"))
	{
		html = _light->GetColorRoom2();
	}
	else if (-1 != url.indexOf("/blink"))
	{
		_light->Blink();
		html = "blink";
	}
	else
	{
		if (-1 != url.indexOf("/startLightIntesity"))
		{
			_light->StartLightResponsive();
		}
		else if (-1 != url.indexOf("/stopLightIntesity"))
		{
			_light->StopLightResponsive();
		}

		html = defaultHtmlPageHeader;


		html += "	function colorChanged(room)	{window.location = 'changeColor?colorH=' + document.getElementById('col'+room).value.substring(1)+'&r='+room;	}	function onLoad(){		document.getElementById('col1').value = '";
		html += _light->GetColorRoom1();
		html += "';document.getElementById('col2').value = '";
		html += _light->GetColorRoom2();
		html += "';	}";
		html += defaultHtmlPageHeaderEnd;

		//color picker
		html += "<input onchange='colorChanged(1)' name = 'colorpicker' id = 'col1' type = 'color'>";
		html += "<input onchange='colorChanged(2)' name = 'colorpicker' id = 'col2' type = 'color'>";
		html += "<a href='startLightIntesity'><button>Start Light responsive</button></a>";
		html += "<a href='stopLightIntesity'><button>Stop Light responsive</button></a>";
		//end line
		html += "<br>";

		html += defaultEndHtmlPage;
	}

	html.toCharArray(_buffer, 1000);

	return _buffer;
}

char* HtmlBuilder::GetNotificationHtml()
{
	String html = "";// String(_notif->GetNotification());
	Serial.println("Html content:");
	Serial.println(String(html));

	html.toCharArray(_buffer, 500);

	return _buffer;
}