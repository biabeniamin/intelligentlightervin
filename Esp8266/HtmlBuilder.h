#pragma once

#include <Arduino.h>
#include "Light.h"


class HtmlBuilder
{
public:
	HtmlBuilder(Light*);

	char* GetHtml(String url);
	char* GetNotificationHtml();

private:
	char *scriptContent = "	function test()	{		alert(document.getElementById('col').value);		window.location = '?colorH=' + document.getElementById('col').value.substring(1);	}	function onLoad(){		document.getElementById('col').value = '#1a6b57';	}";
	char *defaultHtmlPageHeader = "<!DOCTYPE HTML>\r\n<html><head><meta http-equiv='refresh' content='10000' / ><style>button{background-color: #F0CA4D;}</style><script >	";
	char *defaultHtmlPageHeaderEnd = "</script></head><body onload='onLoad()' bgcolor='#324D5C'><center><div style='color:white; background-color: #46B29D; width:80%;font-size:20px;'><h2>Smart lighting</h2>";
	char *defaultEndHtmlPage = "		</div></body></center></html>";

	char _buffer[1000];

	Light *_light;


};