#pragma once

#include <Arduino.h>
#include <FastLED.h>

#define LED_PIN     4
#define NUM_LEDS_ROOM1    12
#define NUM_LEDS_ROOM2    11
#define NUM_LEDS (NUM_LEDS_ROOM1 + NUM_LEDS_ROOM2)
#define BRIGHTNESS  20
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define UPDATES_PER_SECOND 100

struct COLOR_TYPE
{
	char red;
	char green;
	char blue;
};
typedef struct COLOR_TYPE COLOR;

class Light
{
public:
	Light();

	void UpdateRoom1(int, int, int);
	void UpdateRoom2(int, int, int);
	void Update();

	String GetColorRoom1();
	String GetColorRoom2();

	void Blink();
	void CheckTimer();

	void StartLightResponsive();
	void StopLightResponsive();

	

private:
	CRGB _leds[NUM_LEDS];
	COLOR _cRoom1;
	COLOR _cRoom2;

	COLOR _cInitialRoom1;
	COLOR _cInitialRoom2;
	byte _isTimerOn;
	unsigned long _start;
	unsigned long _interval;

	byte _isLightResponsiveOn;

	void CheckLightIntesity();

};