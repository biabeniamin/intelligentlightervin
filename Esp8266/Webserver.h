#include <ESP8266WiFi.h>
#include "HtmlBuilder.h"

class Webserver
{
public:
	Webserver(void (*newClient)());

	void Start(HtmlBuilder*);

	void CheckForConnectedClients();

	String GetParameter(String parameter);

private:
	const char* ssid = "Bia";
	const char* password = "";

	HtmlBuilder *_htmlBuilder;

	WiFiServer *server;

	void MakeHtmlResponse();

	String _request;
	void(*_newClient)();
};
